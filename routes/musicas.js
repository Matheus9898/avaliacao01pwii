var express = require('express');
var router = express.Router();
const dados = require('../dados.json')

//02
router.get('/', function(req, res, next) {
  var limite = req.query.limite;

    if (!limite) {
        limite = 5;
    }

    var teste = [];

    for (let i = 0; i < parseInt(limite); i++) {
        if (i == 0) {
            teste.push({"nome": dados.nome[parseInt(Math.random() * dados.nome.length)]});
        } else if (i == 1){
            teste.push({"cantor": dados.cantor[parseInt(Math.random() * dados.cantor.length)]});
        } else if (i == 2){
            teste.push({"ano": dados.ano[parseInt(Math.random() * dados.ano.length)]});
        } else if (i == 3){
            teste.push({"album": dados.album[parseInt(Math.random() * dados.album.length)]});
        } else if (i == 4){
            teste.push({"tamanho": dados.tamanho[parseInt(Math.random() * dados.tamanho.length)]});
        }
    }

    const musica = {
        "nome": dados.nome[parseInt(Math.random() * dados.nome.length)],
        "cantor": dados.cantor[parseInt(Math.random() * dados.cantor.length)],
        "ano": dados.ano[parseInt(Math.random() * dados.ano.length)],
        "album": dados.album[parseInt(Math.random() * dados.album.length)],
        "tamanho": dados.tamanho[parseInt(Math.random() * dados.tamanho.length)],
    }
    
    res.json(teste);
});

//01
router.get('/random', function(req, res){

  const musica = {
      "nome": dados.nome[parseInt(Math.random() * dados.nome.length)],
      "cantor": dados.cantor[parseInt(Math.random() * dados.cantor.length)],
      "ano": dados.ano[parseInt(Math.random() * dados.ano.length)],
      "album": dados.album[parseInt(Math.random() * dados.album.length)],
      "tamanho": dados.tamanho[parseInt(Math.random() * dados.tamanho.length)],
  }
  
  res.json(musica)
});

//03
router.get('/:propriedade', function(req, res){
  var valor = req.query.valor;
  var propriedade = req.params.propriedade;

  if (!valor ) {
      return res.status(400).json({"erro": "Valor não definido"});
  }
  
  if (propriedade == 'nome') {
      dados.nome.push(valor);
  } else if (propriedade == 'cantor'){
      dados.cantor.push(valor);
  }else if (propriedade == 'ano'){
    dados.ano.push(valor);
  }else if (propriedade == 'album'){
    dados.album.push(valor);
  }else if (propriedade == 'tamanho'){
    dados.tamanho.push(valor);
}
  
  res.json(dados);
})

module.exports = router;


